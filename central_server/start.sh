
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $script_dir

if [ -z $NODE_ENV ]; then
    export NODE_ENV="dev"
fi

port=$1

# if [ -z $port ]; then
#     port=3000
# fi

export PORT=$port

if [ $NODE_ENV == "dev" ]; then
    if [ ! -d node_modules ]; then
        npm i
    fi
    npm run dev
else
    npm run build && cd build/ && npm i && node bin/www
fi