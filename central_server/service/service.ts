import fs from 'fs';
import { PeerData, Peer } from '../model/models';
import axios from 'axios';
import constants from '../utils/constants';
import { PeerStates } from '../utils/constants';


export class PeerService {

    private filePath : string = 'peersData.json';


    constructor() {
        if (!fs.existsSync(this.filePath)) {
            throw new Error('No file containing peers data found!');
        }
    }

    public ping(time : number = constants.HEALTHCHECK_INTERVAL) {
        setInterval(function(scope) {
            return () => scope._pingPeers();
        }(this), time)
    }

    public getPeersData() : PeerData {        
        return <PeerData> JSON.parse(fs.readFileSync(this.filePath, "utf-8"));
    }

    public writePeersData(data : PeerData) {
        fs.writeFileSync(this.filePath, JSON.stringify(data, null, 4));
    }

    public addPeer(peer : Peer) : void {
        let data = this.getPeersData();
        data.peers.push(peer);
        this.writePeersData(data);
    }

    public removePeer(peer : Peer) : void {
        let data = this.getPeersData();
        let index = data.peers.findIndex( (item) => item.url === peer.url);

        if (index === -1) {
            console.log(`No such peer: ${peer.url}!`);
            return;
        }

        data.peers.splice(index, 1);
        this.writePeersData(data);
    }

    
    private async _pingPeers() : Promise<void> {

        let data = this.getPeersData();
        let statusChanged = false;

        for (let i = 0; i < data.peers.length; i++) {

            let peer = data.peers[i];
            let res = null;
            try {
                res = await axios.get(`http://${peer.url}/status`);
            } catch (e) {
                let mes = e.code ? e.code : e;
                console.log();
                console.log(`=========================\nError: ${mes},\nPeer: ${e.config.url}\n=========================\n`)
            }
                        
            if ((!res || res.status !== 200) && peer.status !== PeerStates.DEAD) {
                peer.status = PeerStates.DEAD;
                console.log(`Peer died: ${peer.url}`);
                statusChanged = true;
            } else if (res && res.status === 200 && peer.status !== PeerStates.OK) {                
                peer.status = PeerStates.OK;
                console.log(`Peer alive: ${peer.url}`);
                statusChanged = true;
            }
        }

        if (statusChanged) this.writePeersData(data);
    }
}