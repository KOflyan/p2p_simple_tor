import { PeerStates } from "../utils/constants";


export class Status {
    status : string = PeerStates.OK;
    uptime : number = process.uptime();
}

export class PeerData {
    peers: Peer[] = [];
}

export class Peer {
    url : string;
    status : string;

    constructor(url : string) {
        this.url = url;
        this.status = PeerStates.OK;
    }
}