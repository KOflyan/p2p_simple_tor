import { Inject } from 'typescript-ioc';
import { GET, Path, QueryParam } from 'typescript-rest';
import { Tags } from 'typescript-rest-swagger';
import { Status, PeerData, Peer } from '../model/models';
import { PeerService } from '../service/service';

@Tags('Main ctrl')
@Path('/')
export class Main {

    // @Inject
    // @Context
    // private context : ServiceContext;

    @Inject
    private service : PeerService;


    @GET
    @Path('/status')
    public status() : Status {
        return new Status();
    }

    @GET
    @Path('/add_peer')
    public addPeer(@QueryParam("peer") url : string) : number {
        this.service.addPeer(new Peer(url));
        return 204;
    }

    @GET
    @Path('/get_peers')
    public getPeers() : PeerData {
        return this.service.getPeersData();
    }
}