export default {

    laziness: 0.5,

    port: 1215,

    HEALTHCHECK_INTERVAL: 60000, // 60s
    // HEALTHCHECK_INTERVAL: 10000
}


export enum PeerStates {
    OK = 'OK',
    DEAD = 'DEAD'
}