SIMPLE ANONYMOUS NETWORK IMPLEMENTATION
=======================================

Requirements:
 - bash
 - nmap
 - node

1) Run

```bash
bash deploy.sh
```

2) Make request

Example:
```bash
curl -X 'GET' http://127.0.0.1:3000/download?url=https://ua.all.biz/img/ua/catalog/2027105.jpeg
```

3) Find your file in `node_server/data`
