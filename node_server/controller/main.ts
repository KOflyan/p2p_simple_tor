import { Inject } from 'typescript-ioc';
import {  GET, Path, QueryParam, POST, Context, ServiceContext } from 'typescript-rest';
import { Tags } from 'typescript-rest-swagger';
import { Status, RequestData, FileData } from '../model/models';
import { BadRequestError } from 'typescript-rest/dist/server-errors';
import NodeService from '../service/service';


@Path('/')
@Tags('Main ctrl')
export class Main {


    @Inject
    @Context
    private context : ServiceContext;

    @Inject
    private service : NodeService;

    private static ids : number[] = [];

    @GET
    @Path('/status')
    public status() : Status {
        return new Status();
    }

    @GET
    @Path('/download')
    public async download(@QueryParam('id') id : number, @QueryParam('url') url : string) : Promise<string> {

        let requestId : number = id ? id : this.generateId();

        if (!url) throw new BadRequestError('URL is not specified.');
        if (Main.ids.includes(id)) return 'OK'; // Ignore second request
        url = decodeURI(url);

        // @ts-ignore
        let addr = this.context.request.headers['host'];
        // @ts-ignore
        let forwarder = <string> this.context.request.headers['x-forwarded-from'];

        let peers = await this.service.getPeers();

        if (!forwarder) {  // first node makes request
            Main.ids.push(requestId);
        }

        NodeService.data.push(new RequestData(requestId, forwarder));

        if (forwarder && this.service.isDownload()) {
            this.service.download(requestId, url, addr, forwarder, peers);
        } else {
            this.service.forwardRequest(peers, requestId, url, addr, forwarder);
        }

        return 'OK';
    }

    @POST
    @Path('/file')
    public file(fileData : FileData, @QueryParam('id') id : number) : void {

        if (!id) throw new BadRequestError('Id is absent');

        if (Main.ids && Main.ids.includes(id) && fileData.content) {
            console.log('File req received, saving . . .');

            this.service.save(id, fileData.content, fileData["mime-type"].split('/')[1]);
            NodeService.data = [];
            Main.ids = [];
            return;
        }
        this.service.forwardFileRequest(id, fileData);
    }

    private generateId() : number{
        return Math.floor(Math.random() * Math.pow(10, 10));

    }
}
