import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import { Server } from 'typescript-rest';
import controllers from './controller';

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.disable('x-powered-by');
// @ts-ignore
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// @ts-ignore
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

Server.buildServices(app, ...controllers);


// error handler
app.use( (err : Error, _req : express.Request, res : any, _next : express.NextFunction) => {
  // set locals, only providing error in development
  console.error(err);
  res.status(400).json({error: {
    message: err.message,
    stack: err.stack
  }});
});


// catch 404 and forward to error handler
app.use( (_req : express.Request, res : any, next : express.NextFunction) => {
  if (!res.headersSent) {
    res.status(404).json({err: "Not found"});
  }
  return next();
});

export default app;
