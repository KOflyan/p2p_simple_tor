#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$script_dir" || exit

if [ -z "$NODE_ENV" ]; then
    export NODE_ENV="dev"
fi

port=$1

if [ -z "$port" ]; then
    port=3000
fi

echo

while [ "$(nmap -p $port localhost | tail -n 3 | head -n 1 | cut -d ' ' -f 2)" == "open" ]
do
    new_port=$(( port + 1 ))
    echo ". . . Port $port is busy, trying port $new_port . . ."
    echo
    port=$new_port
done

echo "Selecting port $port"
echo

export PORT=$port


if [ $NODE_ENV == "dev" ]; then
    if [ ! -d node_modules ]; then
        npm i
    fi
    npm run dev
else
    npm run build && cd build/ && npm i && node bin/www
fi
