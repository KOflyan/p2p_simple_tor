import axios from 'axios';
import constants, { PeerState } from '../utils/constants';
import fs from 'fs';
import { Peer, FileData } from '../model/models';
import path from 'path';
import { RequestData } from '../model/models';

const mime = require('mime-types');

export default class NodeService {

    public static data : RequestData[] = [];


    public getPeers() : Promise<Peer[]> {
        return axios.get(`${constants.CENTRAL_SERVER_URL}/get_peers`).then( (res) => res.data.peers);
    }

    public fileExists(id : number) {
        return fs.existsSync(path.join(__dirname, `../data/${id}`));
    }

    public readFile(id : number, type : string = '.html') : string {
        return fs.readFileSync(path.join(__dirname, `../data/${id}${type}`), 'base64');
    }

    public async download(requestId : number, url : string, thisAddr : string, forwarder : string, peers : Peer[]) : Promise<void> {

        console.log(`Node ${thisAddr} is downloading . . .`);

        let extension = path.extname(url);
        let type = mime.lookup(extension);

        if (!type) {
            extension = '.html';
            type = mime.lookup(extension);
        }
        const file = fs.createWriteStream(path.join(__dirname, `../data/${requestId}${extension}`));

        (await axios.get(url, {responseType: 'stream'})).data.pipe(file);
        const f = this.readFile(requestId, extension);
        const payload = new FileData(200, type, f);

        try {
            await this.forwardFileRequest(requestId, payload);
        } catch (e) {

            for (const p of peers) {
                if ([forwarder, thisAddr].includes(p.url)) continue;
                await axios.post(encodeURI(`http://${p.url}/file?id=${requestId}`), payload);
            }
        }
    }

    public forwardRequest(peers : Peer[], requestId : number, url : string, thisAddr : string, forwarder : string) : Promise<any> {

        let response = null;
        peers.forEach( async (p) => {
            if (p.status === PeerState.DEAD || [forwarder, thisAddr].includes(p.url)) return;
            console.log(`Forwarding from ${thisAddr} to --->>> ${p.url}`);

            response = await axios.get(encodeURI(`http://${p.url}/download?id=${requestId}&url=${url}`), {
                headers: {
                    "x-forwarded-from": thisAddr
                }
            });
        });

        return response;
    }

    public forwardFileRequest(requestId : number, payload : FileData) {
        let ind = NodeService.data.findIndex((rq) => rq.id === requestId && rq.forwarder !== null);
        if (ind === -1) return null;
        let forwarder = NodeService.data[ind].forwarder;
        if (!forwarder) return null;


        NodeService.data.splice(ind, 1);
        console.log(`Forwarding back to --->>> ${forwarder}`);

        return axios.post(encodeURI(`http://${forwarder}/file?id=${requestId}`), payload);
    }

    public isDownload() : boolean {
        return Math.random() > constants.LAZINESS;
    }

    public save(id : number, content : string, extension : string) : void {
        fs.writeFileSync(path.join(__dirname, `../data/${id}-000.${extension}`), content, {
            encoding: 'base64'
        });
    }
}
