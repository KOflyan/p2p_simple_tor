export class Status {
    uptime : number = process.uptime();
}

export class RequestData {
    id : number;
    forwarder: string;

    constructor(id? : number, forwarder? : string) {
        this.id = id;
        this.forwarder = forwarder;
    }
}

export class Peer {
    url : string;
    status : string;
}


export class FileData {
    status: number;
    'mime-type': string;
    content : string;

    constructor(status : number, mimeType : string, content : string) {
        this.status = status;
        this["mime-type"] = mimeType;
        this.content = content;
    }
}