

export default {

    LAZINESS: 0.9,

    CENTRAL_SERVER_URL: 'http://127.0.0.1:1215'

}

export enum PeerState {
    OK = 'OK',
    DEAD = 'DEAD'
}
