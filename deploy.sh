#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$script_dir" || exit

if [ ! -f "logs.log" ]; then
    touch logs.log
fi


bash ./central_server/start.sh >> logs.log &
sleep 4
echo "Central server started"

for i in {1..4}; do
    bash ./node_server/start.sh >> logs.log &
    sleep 3
    echo "Node #$i started"
done

tail -f logs.log



